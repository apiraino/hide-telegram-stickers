/*
  Basically a copy and paste from a Mozilla tutorial:
  https://github.com/mdn/webextensions-examples/tree/master/emoji-substitution

  Inspired by Hoda, a really nice friend that loves Telegram stickers too much ;-)
*/

function replaceImages () {
    console.log("[Hide stickers] Global DOM check requested, " + document.images.length + " images found");
    for (var i = 0 ; i < document.images.length; i++) {
        // console.debug("Checking image: " + document.images[i].src);
        replaceImage(document.images[i]);
    }
}

/**
 * Edit <img> when they are stickers
 * Remove the src and add alternate text
 *
 * @param  {Img} node     - The <img> element
 * @return {void}         - Note: the img substitution is done inline
 */
function replaceImage (node) {
    /* Example:

       <div ng-switch-when="sticker"
         my-load-sticker=""
         document="media.document"
         open="true"
         class="clickable"
         style="width: 192px; height: 147px;">
         <img
           alt="[💸 Sticker]"
           style="width: 192px; height: 147px;"
           src="#"
           title="[💸 Sticker]">
       </div>
    */

    if (node.alt && node.alt.includes("Sticker")) {
        // console.debug("FOUND STICKER! " + node.alt);
        // break <img>, show alt text
        node.title = node.alt;
        node.src = "#";
        node.currentSrc = "#";
        node.style = "width:20px; height:20px;";
        // shrink size of the <div> container
        node.parentNode.style.height = "30px";
        // console.debug("Node now:");
        // console.debug(node);
    }
}

// DEBUG: see if the extension is active
// document.body.style.border = "5px solid red";

// Execute on add-on loading and ... (?)
// replaceImages();

// Now monitor the DOM for additions and substitute emoji into new nodes.
// @see https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver.
const observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
        if (mutation.addedNodes && mutation.addedNodes.length > 0) {
            // This DOM change was new nodes being added. Run our substitution
            // algorithm on each newly added node.
            for (let i = 0; i < mutation.addedNodes.length; i++) {
                const newNode = mutation.addedNodes[i];
                // console.debug("New node detected: checking for new stickers ...");
                // console.debug("New node found:");
                // console.debug(newNode);
                replaceImage(newNode);
            }
        }
    });
});
observer.observe(document.body, {
    childList: true,
    subtree: true
});

function notifyExtension(e) {
    // console.debug(">>> Event detected");
    // console.debug(e);
    replaceImages();
}

// triggered when clicking on a channel
window.addEventListener("click", notifyExtension);

// doesn't get triggered (why?)
// document.addEventListener('DOMContentLoaded', notifyExtension);

// Doesn't work because images are not available yet
// window.addEventListener('load', notifyExtension);
