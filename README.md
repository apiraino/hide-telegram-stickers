# Hide Telegram stickers

A Firefox add-on that removes all stickers received when connected on telegram through the <a href="https://web.telegram.org" target="_new">web client</a>.

This add-on only handles the DOM (in a rather crude way) and it **does not inject JavaScript or CSS** into web pages. It is restricted to only work on the Telegram web client page.

**Supports Firefox +57**.

## What it does

Basically scans the DOM and looks for `<img>` HTML entities with "Sticker" in their `alt` attribute, then remove the `src` attribute (so, no, you're not saving bandwidth or anything).

This add-on has been hacked together glueing code at random using the <a href="https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions" target="_new">Mozilla DEV knowledge-base</a>.

## Loading into Firefox

Open a tab in Firefox and go to `about:debugging`, load any file (e.g. manifest.json) from the repo cloned. A red border should appear in your Telegram tab.

Or pack the repo code into a zip file and load this compressed file into Firefox:
``` bash
$ git clone https://gitlab.com/apiraino/hide-telegram-stickers
$ cd hide-telegram-stickers
$ rm -f ~/tmp/hide-telegram-stickers_0.x.zip
$ zip -r -FS ~/tmp/hide-telegram-stickers_0.x.zip . -i manifest.json icons/* substitute.js
```

or use the pre-packaged `.xpi` file. Build and sign setup is described
[here](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Package_your_extension_).

I use `web-ext sign` - see [Mozilla web-ext](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/web-ext_command_reference#web-ext_sign).

## TODO

* improve code because it's randomly bashing at the DOM
* BUG: DOM manipulating works reasonably well when the page is completely
  loaded. However, when reloading the page, stickers do not disappear and are only resized

## Why

First there were IRC-styled emotions (_/me feels lazy today_), shortcuts (`<g>` = grin) and HTML tags (`<rant>Why oh whyyyyy</rant>`).

Then there were <a href="https://en.wikipedia.org/wiki/List_of_emoticons" target="_new">text smileys</a>, a rich set of emotions easier to understand, still people were required to tilt their heads.

Then the first chat applications (IM) came to existence, and the new wave of users required software makers to lower the bar: so GIFs and other obscure proprietary mess, <a href="https://duckduckgo.com/?q=AOL+emoticons" target="_new">ugly as hell</a>.

Then there were <a href="https://en.wikipedia.org/wiki/Emoji" target="_new">UTF-8 emojis</a> (standardization FTW). Also, thousands of them, because people were anxious <a href="https://en.wikipedia.org/wiki/Newspeak" target="_new">to reduce and deprive a lot more concepts</a> to a single character.

And finally: <a href="https://en.wikipedia.org/wiki/Telegram_(service)#Stickers" target="_new">Telegram stickers</a>, the utmost un-standard, bandwitdh-wasting, silly and unnerving pervertion humanity has ever conceived, until the next one.

This has to stop. Now.
