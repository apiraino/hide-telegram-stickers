#!/bin/bash

# your Firefox API credentials
WEB_EXT_API_KEY="xxx:yyy"
WEB_EXT_API_SECRET="abcdef123456"

APIKEY="$WEB_EXT_API_KEY"
APISECRET="$WEB_EXT_API_SECRET"
unset WEB_EXT_API_KEY
unset WEB_EXT_API_SECRET

# Zip the Firefox extension.
npx web-ext build --config-discovery=false

# Zip+sign (XPI) the Firefox extension.
npx web-ext sign \
  --api-key=$APIKEY \
  --api-secret=$APISECRET \
  --config-discovery=false

exit 0
